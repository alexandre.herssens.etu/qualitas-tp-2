package fr.univlille.iut.info.r402.frenchvat;

import java.util.Map;
import java.util.Objects;

public class TicketLine {
    private String designation;
    private double priceIncVat;
    private int quantity;
    private VAT vat;
    private double amountOfVat;

    public TicketLine(String designation, double priceWithoutTax, int quantity, VAT vat, double amountOfVat) {
        this.designation = designation;
        this.priceIncVat = priceWithoutTax;
        this.quantity = quantity;
        this.vat = vat;
        this.amountOfVat = amountOfVat;
    }



}
