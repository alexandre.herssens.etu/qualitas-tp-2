package fr.univlille.iut.info.r402.frenchvat;

public enum VAT {
    Normal(20), Intermediate(10), Reduced(5.5), Particular(2.1);

    private double rate;

    VAT(double rate) {
        this.rate = rate;
    }

}
