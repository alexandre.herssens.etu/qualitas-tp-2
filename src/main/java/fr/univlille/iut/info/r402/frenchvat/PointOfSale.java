package fr.univlille.iut.info.r402.frenchvat;

import java.util.Objects;

public final class PointOfSale {
    private final String name;
    private final boolean withServicesToEatOnSite;
    private final Department department;

    PointOfSale(String name, boolean withServicesToEatOnSite, Department department) {

        this.name = name;
        this.withServicesToEatOnSite = withServicesToEatOnSite;
        this.department = department;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PointOfSale that = (PointOfSale) o;
        return withServicesToEatOnSite == that.withServicesToEatOnSite && Objects.equals(name, that.name) && department == that.department;
    }


    @Override
    public String toString() {
        return "PointOfSale{" + "name='" + name + '\'' + ", withServicesToEatOnSite=" + withServicesToEatOnSite + ", department=" + department + '}';
    }
}
