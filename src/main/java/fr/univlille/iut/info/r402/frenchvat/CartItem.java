package fr.univlille.iut.info.r402.frenchvat;

public final class CartItem {
    private String productName;
    private double allTaxesIncludedPrice;
    private boolean isFrozen;
    private boolean couldBeStored;
    private boolean containsOpenSeafood;
    private int quantity;

    public CartItem(String productName, double allTaxesIncludedPrice, boolean isFrozen, boolean couldBeStored, boolean containsOpenSeafood, int quantity) {
        this.productName = productName;
        this.allTaxesIncludedPrice = allTaxesIncludedPrice;
        this.isFrozen = isFrozen;
        this.couldBeStored = couldBeStored;
        this.containsOpenSeafood = containsOpenSeafood;
        this.quantity = quantity;
    }
}
