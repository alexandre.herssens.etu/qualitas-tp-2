package fr.univlille.iut.info.r402.frenchvat;

public enum Department {
    Nord, CorseDuNord, CorseDuSud, Guadeloupe, Martinique, Réunion, Guyane, Mayotte;
}
